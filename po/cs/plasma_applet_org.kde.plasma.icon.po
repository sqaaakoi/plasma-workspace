# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2017, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-09 02:14+0000\n"
"PO-Revision-Date: 2020-06-04 11:18+0200\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 20.04.1\n"

#: iconapplet.cpp:93
#, kde-format
msgid "Failed to create icon widgets folder '%1'"
msgstr "Vytvoření složky s widgety ikon '%1' selhalo"

#: iconapplet.cpp:139
#, kde-format
msgid "Failed to copy icon widget desktop file from '%1' to '%2'"
msgstr ""

#: iconapplet.cpp:386
#, kde-format
msgid "Open Containing Folder"
msgstr "Otevřít odpovídající složku"

#: iconapplet.cpp:567
#, kde-format
msgid "Properties for %1"
msgstr "Vlastnosti pro %1"

#: package/contents/ui/main.qml:50
#, kde-format
msgid "Properties"
msgstr "Vlastnosti"
