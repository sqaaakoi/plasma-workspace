# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2014, 2017, 2019.
# Vit Pelcak <vit@pelcak.org>, 2021, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-20 02:59+0000\n"
"PO-Revision-Date: 2023-06-14 14:55+0200\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 23.04.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Vít Pelčák"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "vit@pelcak.org"

#: calendar/eventdatadecorator.cpp:51
#, kde-format
msgctxt "Agenda listview section title"
msgid "Holidays"
msgstr "Svátky"

#: calendar/eventdatadecorator.cpp:53
#, kde-format
msgctxt "Agenda listview section title"
msgid "Events"
msgstr "Události"

#: calendar/eventdatadecorator.cpp:55
#, kde-format
msgctxt "Agenda listview section title"
msgid "Todo"
msgstr "Úkoly"

#: calendar/eventdatadecorator.cpp:57
#, kde-format
msgctxt "Means 'Other calendar items'"
msgid "Other"
msgstr "Jiné"

#: calendar/qml/DayDelegate.qml:44
#, kde-format
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "%1 událost"
msgstr[1] "%1 události"
msgstr[2] "%1 událostí"

#: calendar/qml/DayDelegate.qml:44
#, kde-format
msgid "No events"
msgstr "Žádné události"

#: calendar/qml/MonthViewHeader.qml:68
#, kde-format
msgctxt "Format: month year"
msgid "%1 %2"
msgstr "%1 %2"

#: calendar/qml/MonthViewHeader.qml:114
#, kde-format
msgid "Days"
msgstr "Dnů"

#: calendar/qml/MonthViewHeader.qml:120
#, kde-format
msgid "Months"
msgstr "Měsíce"

#: calendar/qml/MonthViewHeader.qml:126
#, kde-format
msgid "Years"
msgstr "Roky"

#: calendar/qml/MonthViewHeader.qml:164
#, kde-format
msgid "Previous Month"
msgstr "Předchozí měsíc"

#: calendar/qml/MonthViewHeader.qml:166
#, kde-format
msgid "Previous Year"
msgstr "Předchozí rok"

#: calendar/qml/MonthViewHeader.qml:168
#, kde-format
msgid "Previous Decade"
msgstr "Předchozí desetiletí"

#: calendar/qml/MonthViewHeader.qml:185
#, kde-format
msgctxt "Reset calendar to today"
msgid "Today"
msgstr "Dnes"

#: calendar/qml/MonthViewHeader.qml:186
#, kde-format
msgid "Reset calendar to today"
msgstr "Resetovat kalendář na dnešek"

#: calendar/qml/MonthViewHeader.qml:197
#, kde-format
msgid "Next Month"
msgstr "Příští měsíc"

#: calendar/qml/MonthViewHeader.qml:199
#, kde-format
msgid "Next Year"
msgstr "Příští rok"

#: calendar/qml/MonthViewHeader.qml:201
#, kde-format
msgid "Next Decade"
msgstr "Následující desetiletí"

#: calendar/qml/MonthViewHeader.qml:257
#, kde-format
msgid "Keep Open"
msgstr "Ponechat otevřené"

#: containmentlayoutmanager/qml/BasicAppletContainer.qml:275
#, kde-format
msgid "Configure…"
msgstr "Nastavit…"

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:10
#, kde-format
msgid "Screen lock enabled"
msgstr "Zámek obrazovky povolen"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:11
#, kde-format
msgid "Sets whether the screen will be locked after the specified time."
msgstr "Nastavuje, zda bude zámek obrazovky po daném čase povolen."

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:16
#, kde-format
msgid "Screen saver timeout"
msgstr "Prodleva spořiče obrazovky"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:17
#, kde-format
msgid "Sets the minutes after which the screen is locked."
msgstr "Nastavuje počet minut, po jejichž uplynutí se zamkne obrazovka."

#: sessionsprivate/sessionsmodel.cpp:235 sessionsprivate/sessionsmodel.cpp:239
#, kde-format
msgid "New Session"
msgstr "Nové sezení"

#: shellprivate/widgetexplorer/kcategorizeditemsviewmodels.cpp:64
#, kde-format
msgid "Filters"
msgstr "Filtry"

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:41
#, kde-format
msgid "Select Plasmoid File"
msgstr "Vyberte soubor plasmoidu"

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:65
#, kde-format
msgid "Installing the package %1 failed."
msgstr "Instalace balíčku %1 selhala."

#: shellprivate/widgetexplorer/openwidgetassistant.cpp:65
#, kde-format
msgid "Installation Failure"
msgstr "Chyba instalace"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:112
msgctxt "applet category"
msgid "Accessibility"
msgstr "Zpřístupnění"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:113
msgctxt "applet category"
msgid "Application Launchers"
msgstr "Spouštěče aplikací"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:114
msgctxt "applet category"
msgid "Astronomy"
msgstr "Astronomie"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:115
msgctxt "applet category"
msgid "Date and Time"
msgstr "Datum a čas"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:116
msgctxt "applet category"
msgid "Development Tools"
msgstr "Vývojové nástroje"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:117
msgctxt "applet category"
msgid "Education"
msgstr "Výuka"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:118
msgctxt "applet category"
msgid "Environment and Weather"
msgstr "Prostředí a počasí"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:119
msgctxt "applet category"
msgid "Examples"
msgstr "Příklady"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:120
msgctxt "applet category"
msgid "File System"
msgstr "Souborový systém"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:121
msgctxt "applet category"
msgid "Fun and Games"
msgstr "Zábava a hry"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:122
msgctxt "applet category"
msgid "Graphics"
msgstr "Grafika"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:123
msgctxt "applet category"
msgid "Language"
msgstr "Jazyk"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:124
msgctxt "applet category"
msgid "Mapping"
msgstr "Mapování"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:125
msgctxt "applet category"
msgid "Miscellaneous"
msgstr "Různé"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:126
msgctxt "applet category"
msgid "Multimedia"
msgstr "Multimédia"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:127
msgctxt "applet category"
msgid "Online Services"
msgstr "Služby online"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:128
msgctxt "applet category"
msgid "Productivity"
msgstr "Produktivita"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:129
msgctxt "applet category"
msgid "System Information"
msgstr "Systémové informace"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:130
msgctxt "applet category"
msgid "Utilities"
msgstr "Nástroje"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:131
msgctxt "applet category"
msgid "Windows and Tasks"
msgstr "Okna a úlohy"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:132
msgctxt "applet category"
msgid "Clipboard"
msgstr "Schránka"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:133
msgctxt "applet category"
msgid "Tasks"
msgstr "Úlohy"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:149
#, kde-format
msgid "All Widgets"
msgstr "Všechny widgety"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:153
#, kde-format
msgid "Running"
msgstr "Běžící"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:159
#, kde-format
msgctxt ""
"@item:inmenu used in the widget filter. Filter widgets that can be un-"
"installed from the system, which are usually installed by the user to a "
"local place."
msgid "Uninstallable"
msgstr "Odinstalovatelné"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:163
#, kde-format
msgid "Categories:"
msgstr "Kategorie: "

#: shellprivate/widgetexplorer/widgetexplorer.cpp:235
#, kde-format
msgid "Download New Plasma Widgets"
msgstr "Stáhnout nové widgety Plasma"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:244
#, kde-format
msgid "Install Widget From Local File…"
msgstr "Instalovat widget ze souboru…"
